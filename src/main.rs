use std::process::Command;

fn main() {

     Command::new("cmd")
             .args(&["/C", "calc.exe"])
             .output()
             .expect("failed to execute process");
}
