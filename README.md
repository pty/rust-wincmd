# Execute Commands on Windows using Rust

# Dependencies

```
apt install mingw-w64
```

# Prepare

```
rustup target add x86_64-pc-windows-gnu
```

# Build

```
cargo build --release --target x86_64-pc-windows-gnu
```
